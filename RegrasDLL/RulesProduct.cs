﻿/**
 * Author: Carlos Pereira, Fábio Rodrigues, Rui Miranda
 * Date: 17/01/2022
 * Number: 6498, 20079, 21135
 * Version: 1.0
 * Description: Feed Zero Waste
 * */

using System;
using System.Collections.Generic;
using BusinessObjectsDLL;
using DadosDLL;

namespace RegrasDLL
{
    [Serializable]
    /// <summary>
    /// Classe que contém regras para produtos
    /// </summary>
    public class RulesProduct
    {
        /// <summary>
        /// Método que procura por um produto dado um identificador de produtos
        /// </summary>
        /// <param name="id">Identificador de produto</param>
        /// <returns></returns>
        public static Product Find(int id)
        {
            return Products.FindProduct(id);
        }

        /// <summary>
        /// Método que permite inserir um novo produto na lista
        /// </summary>
        /// <param name="c">Casa a inserir</param>
        /// <returns></returns>
        public static bool Insert(Product c)
        {
            bool a = Products.InsertProduct(c);
            if (a)
                return true;
            else
                throw new Exception("Product Already Exists!");            
        }

        /// <summary>
        /// Método que permite remover da lista de produtos um determinado produto
        /// </summary>
        /// <param name="c">Produto a remover</param>
        /// <returns></returns>
        public static bool Remove(Product c)
        {
            bool a = Products.RemoveProduct(c);
            if (a)
                return true;
            else
                throw new Exception("Error: Product doesn't exist!");
        }

        /// <summary>
        /// Método para listar todos os produtos da lista
        /// </summary>
        public static Dictionary<string, List<Product>> Listar()
        {
            return Products.Lista();
        }

        /// <summary>
        /// Método que grava em ficheiro a lista de produtos
        /// </summary>
        /// <param name="fileName">Nome do ficheiro</param>
        /// <returns></returns>
        public static bool Save(string fileName)
        {
            return Products.GravaFicheiro(fileName);
        }

        /// <summary>
        /// Método que permite carregar ficheiro de produtos guardado
        /// </summary>
        /// <param name="fileName">Nome do ficheiro</param>
        /// <returns></returns>
        public static bool Load(string fileName)
        {
            return Products.CarregaFicheiro(fileName);
        }
    }
}