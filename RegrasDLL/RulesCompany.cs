﻿/**
 * Author: Carlos Pereira, Fábio Rodrigues, Rui Miranda
 * Date: 17/01/2022
 * Number: 6498, 20079, 21135
 * Version: 1.0
 * Description: Feed Zero Waste
 * */

using System;
using System.Collections.Generic;
using BusinessObjectsDLL;
using DadosDLL;

namespace RegrasDLL
{
    [Serializable]
    /// <summary>
    /// Classe que contém regras para empresas
    /// </summary>
    public class RulesCompany
    {
        /// <summary>
        /// Método que procura por uma empresa
        /// </summary>
        /// <param name="name">Nome da Empresa</param>
        /// <returns></returns>
        public static Company Find(string name)
        {
            return Companies.FindCompany(name);
        }

        /// <summary>
        /// Método que permite inserir uma nova empresa na lista
        /// </summary>
        /// <param name="c">Empresa a inserir</param>
        /// <returns></returns>
        public static bool Insert(Company c)
        {
            return Companies.InsertCompany(c);
        }

        /// <summary>
        /// Método que permite remover da lista de empresas uma determinada empresa
        /// </summary>
        /// <param name="c">Empresa a remover</param>
        /// <returns></returns>
        public static bool Remove(Company c)
        {
            return Companies.RemoveCompany(c);
        }
  
        /// <summary>
        /// Método para listar todas as empresas da lista
        /// </summary>
        public static List<Company> List()
        {
            return Companies.Lista();
        }

        /// <summary>
        /// Método que grava em ficheiro a lista de empresas
        /// </summary>
        /// <param name="fileName">Nome do ficheiro</param>
        /// <returns></returns>
        public static bool Gravar(string fileName)
        {
            return Companies.GravaFicheiro(fileName);
        }

        /// <summary>
        /// Método que permite carregar ficheiro de empresas guardado
        /// </summary>
        /// <param name="fileName">Nome do ficheiro</param>
        /// <returns></returns>
        public static bool Carregar(string fileName)
        {
            return Companies.CarregaFicheiro(fileName);
        }
    }
}
