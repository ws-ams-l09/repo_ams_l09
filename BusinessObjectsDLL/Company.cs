﻿/**
 * Author: Carlos Pereira, Fábio Rodrigues, Rui Miranda
 * Date: 17/01/2022
 * Number: 6498, 20079, 21135
 * Version: 1.0
 * Description: Feed Zero Waste
 * */

using System;
using DadosDLL;

namespace BusinessObjectsDLL
{
    [Serializable]
    /// <summary>
    /// Classe que define uma empresa
    /// </summary>
    public class Company : IFunctions, IComparable<Company>
    {
        #region Attributes
        static int totCompanies;
        int idCompany;
        string nameComp;
        string contactComp;
        string compAddress;
        string descComp;
        string cp;
        #endregion

        #region Constructors
        /// <summary>
        /// Método estático que inicializa a variável total de empresas a zero
        /// </summary>
        static Company()
        {
            totCompanies = 0;
        }
        /// <summary>
        /// Construtor que permite instanciar objetos sem parâmetros
        /// </summary>
        public Company()
        {
            totCompanies++;
            idCompany = totCompanies;
        }

        /// <summary>
        /// Construtor de classe que permite instanciar objetos com os atributos inicializados 
        /// </summary>
        /// <param name="nameComp">Nome da Empresa</param>
        /// <param name="contactComp">Contacto da Empresa</param>
        /// <param name="compAddress">Morada da Empresa</param>
        /// <param name="descComp">Descrição da Empresa</param>
        /// <param name="CP">Código Postal</param>
        public Company(string nameComp, string contactComp, string compAddress, string descComp, string cp)
        {
            totCompanies++;
            idCompany = totCompanies;
            this.nameComp = nameComp;
            this.contactComp = contactComp;
            this.compAddress = compAddress;
            this.descComp = descComp;
            this.cp = cp;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Propriedade do atributo identificador de empresa, apenas pode ser lido
        /// </summary>    
        public int IdCompany
        {
            get { return idCompany; }
        }

        /// <summary>
        /// Propriedade do nome da empresa
        /// </summary>
        public string NameComp
        {
            get { return nameComp; }
            set { nameComp = value; }
        }

        /// <summary>
        /// Propriedade do atributo contacto da empresa
        /// </summary>
        public string ContactComp
        {
            get { return contactComp; }
            set { contactComp = value; }
        }

        /// <summary>
        /// Propriedade do atributo morada da empresa
        /// </summary>
        public string CompAddress
        {
            get { return compAddress; }
            set { compAddress = value; }
        }

        /// <summary>
        /// Propriedade do atributo descrição da empresa
        /// </summary>
        public string DescComp
        {
            get { return descComp; }
            set { descComp = value; }
        }

        /// <summary>
        /// Propriedade do atributo código postal da empresa
        /// </summary>
        public string CP
        {
            get { return cp; }
            set { cp = value; }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Redefinição do método ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("ID Company: {0} - Name: {1} - Contact: {2} - Address: {3}" +
                "Description: {4} - CP: {5}", idCompany, nameComp, contactComp, compAddress, descComp, cp));
        }

        /// <summary>
        /// Redefinição do método Equals, altera a forma de como comparamos dois objetos 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            Company c = obj as Company;
            return this.IdCompany == c.IdCompany;
        }
        /// <summary>
        /// Redefinição do método GetHashCode, importante no tratamento de coleções HashTable
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return idCompany;
        }
        #endregion

        #region Operators
        /// <summary>
        /// Redefinição do operador ==
        /// </summary>
        /// <param name="c">Company</param>
        /// <returns></returns>
        public static bool operator ==(Company c1, Company c2)
        {
            return c1.Equals(c2);
        }
        /// <summary>
        /// Redefinição do operador !=
        /// </summary>
        /// <param name="c">Company</param>
        /// <returns></returns>
        public static bool operator !=(Company c1, Company c2)
        {
            return !(c1 == c2);
        }
        #endregion

        #region Methods

        /// <summary>
        /// Método que permite comparar objetos
        /// </summary>
        /// <param name="c">Company</param>
        /// <returns></returns>
        public int CompareTo(Company c)
        {
            Company aux = c;
            return (aux.IdCompany.CompareTo(c.IdCompany));
        }
        
        /// <summary>
        /// Método que verifica se o cliente foi inserido com sucesso
        /// </summary>
        /// <param name="c">Company</param>
        /// <returns></returns>
        public bool Regista()
        {
            bool b = Companies.InsertCompany(this);
            if (b)
                return true;
            else
                throw new Exception("Company already exists!");
        }
        
        /// <summary>
        /// Método que testa se o cliente foi removido com sucesso
        /// </summary>
        /// <param name="c">Company</param>
        /// <returns></returns>
        public bool Apaga()
        {
            bool b = Companies.RemoveCompany(this);
            if (b)
                return true;
            else
                return false;
        }
        
        /// <summary>
        /// Método que testa se o ficheiro foi gravado com sucesso
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool Grava(string s)
        {
            bool b = Companies.GravaFicheiro(s);
            if (b)
                return true;
            else
                return false;
        }
        #endregion

        #region Destructor
        ~Company()
        { }
        #endregion
    }
}
