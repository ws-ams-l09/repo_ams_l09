﻿/**
 * Author: Carlos Pereira, Fábio Rodrigues, Rui Miranda
 * Date: 17/01/2022
 * Number: 6498, 20079, 21135
 * Version: 1.0
 * Description: Feed Zero Waste
 * */

namespace BusinessObjectsDLL
{
    /// <summary>
    /// Interface que contem os métodos obrigatórios para gerir clientes e casas
    /// </summary>
    public interface IFunctions
    {
        /// <summary>
        /// Método a implementar de inserção de objetos
        /// </summary>
        /// <param name="c">Qualquer objeto</param>
        /// <returns></returns>
        bool Regista();

        /// <summary>
        /// Método de interface para remover objetos
        /// </summary>
        /// <param name="c">Qualquer objeto</param>
        /// <returns></returns>
        bool Apaga();

        /// <summary>
        /// Interface que permite gravar para ficheiro binário
        /// </summary>
        /// <param name="s">Nome do ficheiro</param>
        /// <returns></returns>
        bool Grava(string s);
    }
}
