﻿/**
 * Author: Carlos Pereira, Fábio Rodrigues, Rui Miranda
 * Date: 17/01/2022
 * Number: 6498, 20079, 21135
 * Version: 1.0
 * Description: Feed Zero Waste
 * */

using System;
using DadosDLL;

namespace BusinessObjectsDLL
{
    [Serializable]
    /// <summary>
    /// Classe que define um produto
    /// </summary>
    public class Product : IFunctions
    {
        #region Attributes
        static int totProducts;
        int idProduct;
        string prodName;
        string descName;
        string type;
        int quantity;
        DateTime bestBeforeDate;
        #endregion

        #region Constructors
        
        /// <summary>
        /// Método para inicializar o identificador de produto
        /// </summary>
        static Product()
        {
            totProducts = 0;
        }

        /// <summary>
        /// Construtor da classe Product com parâmetros
        /// </summary>
        public Product(string prodName, string descName, string type, int quantity, DateTime bestBeforeDate)
        {
            totProducts++;
            idProduct = totProducts;
            this.prodName = prodName;
            this.descName = descName;
            this.type = type;
            this.quantity = quantity;
            this.bestBeforeDate = bestBeforeDate;
        }

        /// <summary>
        /// Construtor sem parâmetros
        /// </summary>
        public Product()
        {
            totProducts++;
            idProduct = totProducts;   
        }
        #endregion

        #region Properties
        /// <summary>
        /// Propriedade do atributo identificador de produto
        /// </summary>
        public int IdProduct
        {
            get => idProduct;
        }
        /// <summary>
        /// Propriedade do atributo de nome do produto
        /// </summary>
        public string ProdName
        {
            get => prodName;
            set => prodName = value;
        }
        /// <summary>
        /// Propriedade do atributo de descrição do produto
        /// </summary>
        public string DescName
        {
            get => descName;
            set => descName = value;
        }
        /// <summary>
        /// Propriedade do atributo tipo de produto
        /// </summary>
        public string Type
        {
            get => type;
            set => type = value;
        }
        /// <summary>
        /// Propriedade do atributo quantidade
        /// </summary>
        public int Quantity
        {
            get => quantity;
            set => quantity = value;
        }
        /// <summary>
        /// Propriedade do atributo data de preferência de consumo do produto
        /// </summary>
        public DateTime BestBeforeDate
        {
            get => bestBeforeDate;
            set => bestBeforeDate = value;
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Redefinição do método ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("ID Product: {0} - Name: {1} - Description: {2} - Type: {3} - " +
                "Quantity: {4} - Best Before Date: {5}", 
                idProduct, prodName, descName, type, quantity, bestBeforeDate));
        }

        /// <summary>
        /// Redefinição do método Equals, altera a forma de como comparamos dois objetos 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            Product c = obj as Product;
            return this.IdProduct == c.IdProduct;
        }
        /// <summary>
        /// Redefinição do método GetHashCode, importante no tratamento de coleções HashTable
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return idProduct;
        }
        #endregion

        #region Operators
        /// <summary>
        /// Redefinição do operador ==
        /// </summary>
        /// <param name="c">Product</param>
        /// <returns></returns>
        public static bool operator ==(Product c1, Product c2)
        {
            return c1.Equals(c2);
        }
        /// <summary>
        /// Redefinição do operador !=
        /// </summary>
        /// <param name="c">Product</param>
        /// <returns></returns>
        public static bool operator !=(Product c1, Product c2)
        {
            return !(c1 == c2);
        }
        #endregion

        #region Other Methods
        
        /// <summary>
        /// Método que testa se foi possível inserir um produto
        /// </summary>
        /// <param name="c">Product</param>
        /// <returns></returns>
        public bool Regista()
        {
            bool b = Products.InsertProduct(this);
            if (b)
                return true;
            else
                return false;
        }
        
        /// <summary>
        /// Método que testa se o produto foi removido com sucesso
        /// </summary>
        /// <param name="c">Product</param>
        /// <returns></returns>
        public bool Apaga()
        {
            bool b = Products.RemoveProduct(this);
            if (b)
                return true;
            else
                return false;
        }
        
        /// <summary>
        /// Método que testa se foi gravado no ficheiro com sucesso
        /// </summary>
        /// <param name="s">Nome do ficheiro</param>
        /// <returns></returns>
        public bool Grava(string s)
        {
            bool b = Products.GravaFicheiro(s);
            if (b)
                return true;
            else
                return false;
        }
        #endregion

        #region Destructor
        ~Product()
        { }
        #endregion
    }
}
