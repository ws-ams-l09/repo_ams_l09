﻿ /**
 * Author: Carlos Pereira, Fábio Rodrigues, Rui Miranda
 * Date: 17/01/2022
 * Number: 6498, 20079, 21135
 * Version: 1.0
 * Description: Feed Zero Waste
 * */

using System;
using System.Collections.Generic;
using BusinessObjectsDLL;
using RegrasDLL;

namespace FrontEndDLL
{
    /// <summary>
    /// Classe que permite interagir com o utilizador
    /// </summary>
    public class FrontEnd
    {
        public static void Menu()
        {
            int opt;
            do
            {               
                Console.WriteLine("Feed Zero Waste");
                Console.WriteLine();
                Console.WriteLine("1 - Gerir Produtos");
                Console.WriteLine("0 - Sair");
                Console.WriteLine();

                Console.Write("Escolha uma opção: ");
                opt = int.Parse(Console.ReadLine());
                Console.Clear();

                switch (opt)
                {
                    case 1:
                        MenuProduct();
                        break;

                    case 0:
                        break;

                    default:
                        throw new Exception("Erro: Opção Inválida");
                }
            } while (opt != 0);     
        }

        /// <summary>
        /// Método que mostra o menu da opção gerir produtos 
        /// </summary>
        public static void MenuProduct()
        {
            string fileName = @"C:\temp\Products.bin";
            int opt;
            Dictionary<string, List<Product>> products = new Dictionary<string, List<Product>>();
            Product cs;

            bool y = RulesProduct.Load(fileName);
            if (y) Console.WriteLine("Ficheiro carregado com sucesso!");
            else Console.WriteLine("Não foi possível carregar ficheiro!");
            Console.WriteLine();

            do
            {
                Console.WriteLine("1 - Inserir Produto");
                Console.WriteLine("2 - Remover Produto");
                Console.WriteLine("3 - Listar Produtos");
                Console.WriteLine("0 - Voltar ao Menu Principal");
                Console.WriteLine();
                Console.Write("Opção: ");
                opt = int.Parse(Console.ReadLine());
                Console.Clear();

                switch (opt)
                {
                    case 1:
                        cs = new Product();
                        Console.Write("Nome do Produto: ");
                        cs.ProdName = Console.ReadLine();
                        Console.Write("Descrição do Produto: ");
                        cs.DescName = Console.ReadLine();
                        Console.Write("Tipo de Produto: ");
                        cs.Type = Console.ReadLine();
                        Console.Write("Quantidade: ");
                        cs.Quantity = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Data de Preferência de Consumo: ");
                        cs.BestBeforeDate = DateTime.Parse(Console.ReadLine());

                        bool c = false;                      
                        c = RulesProduct.Insert(cs);
                        if (c)
                            Console.WriteLine("Produto inserido com sucesso!");
                        else
                            Console.WriteLine("Não foi possível inserir o produto!");
                        Console.WriteLine();
                        break;

                    case 2:
                        Console.Write("ID do produto que pretende remover: ");
                        int id = int.Parse(Console.ReadLine());
                    
                        cs = RulesProduct.Find(id);
                        bool d = RulesProduct.Remove(cs);
                        if (d)
                            Console.WriteLine("Produto removido com sucesso!");
                        else
                            Console.WriteLine("Não foi possível remover o produto!");
                        Console.WriteLine();
                        break;

                    case 3:
                        products = RulesProduct.Listar();
                        foreach (var k in products)
                        {
                            foreach (var product in k.Value)
                            {
                                Console.WriteLine("ID Produto: {0}", product.IdProduct);
                                Console.WriteLine("Nome do Produto: {0}", product.ProdName);
                                Console.WriteLine("Descrição do Produto: {0}", product.DescName);
                                Console.WriteLine("Tipo de Produto: {0}", product.Type);
                                Console.WriteLine("Quantidade: {0}", product.Quantity);
                                Console.WriteLine("Data de Preferência de Consumo: {0}", product.BestBeforeDate);
                                Console.WriteLine();
                            }
                        }
                        Console.WriteLine();
                        break;

                    case 0:
                        break;

                    default:
                        throw new Exception("ERRO: Opção errada!");
                }
            } while (opt != 0);

            bool f = RulesProduct.Save(fileName);
            if (f)
                Console.WriteLine("Ficheiro gravado com sucesso!");
            else
                Console.WriteLine("Não foi possível gravar!");
            Console.WriteLine();
        }
    }
}
