﻿/**
 * Author: Carlos Pereira, Fábio Rodrigues, Rui Miranda
 * Date: 17/01/2022
 * Number: 6498, 20079, 21135
 * Version: 1.0
 * Description: Feed Zero Waste
 * */

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using BusinessObjectsDLL;

namespace DadosDLL
{
    [Serializable]
    /// <summary>
    /// Classe que permite gerir uma lista de produtos
    /// </summary>
    public class Products
    { 
        #region Attributes
        static Dictionary<string, List<Product>> products;
        #endregion

        #region Constructors
        /// <summary>
        /// Método estático para definir uma lista de Produto
        /// </summary>
        static Products()
        {
            products = new Dictionary<string, List<Product>>();
        }
        #endregion

        #region Properties
        #endregion

        #region Overrides
        #endregion

        #region Operators
        #endregion

        #region Other Methods

        /// <summary>
        /// Método que procura por um produto, dado um identificador de produto
        /// </summary>
        /// <param name="id">Id do Produto</param>
        /// <returns></returns>
        public static Product FindProduct(int id)
        {
            foreach (var k in products)
            {
                foreach (var prod in k.Value)
                {
                    if (prod.IdProduct == id)
                        return prod;
                }
            }
            return null;
        }

        /// <summary>
        /// Método que verifica se um determinado produto já existe na lista
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool ExistsProduct(string name)
        {
            foreach (var k in products)
            { 
                foreach (var prod in k.Value)
                {
                    if (prod.ProdName == name)
                        return true;
                }
            }
            return false;
        }
   
        /// <summary>
        /// Método que permite inserir um novo produto
        /// </summary>
        /// <param name="c">Produto a inserir</param>
        /// <returns></returns>
        public static bool InsertProduct(Product c)
        {
            if (!products.ContainsKey(c.ProdName))    //se a chave ainda não existe
                products.Add(c.ProdName, new List<Product>());

            if (!ExistsProduct(c.ProdName))
            {
                products[c.ProdName].Add(c);
                return true;                
            }
            return false;
        }
        
        /// <summary>
        /// Método que permite remover da lista de produtos um determinado produto
        /// </summary>
        /// <param name="c">Produto a remover</param>
        /// <returns></returns>
        public static bool RemoveProduct(Product c)
        {
            if (products.ContainsKey(c.ProdName))
            {
                foreach (var k in products)
                {
                    foreach (var prod in k.Value)
                    {
                        if (prod.IdProduct == c.IdProduct)
                        {
                            products[c.ProdName].Remove(c);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Método para listar todos os produtos
        /// </summary>
        public static Dictionary<string, List<Product>> Lista()
        {
            return products;
        }
        
        /// <summary>
        /// Método que grava lista de produtos para um ficheiro binário
        /// </summary>
        /// <param name="fileName">Nome do ficheiro</param>
        /// <returns></returns>
        public static bool GravaFicheiro(string fileName)
        {
            try
            {
                Stream st = File.Open(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(st, products);
                st.Close();
                return true;
            }
            catch (IOException e)
            {
                throw e;
            }

        }

        /// <summary>
        /// Método que permite carregar conteúdo de ficheiro binário
        /// </summary>
        /// <param name="fileName">Nome do ficheiro</param>
        /// <returns></returns>
        public static bool CarregaFicheiro(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    Stream st= File.Open(fileName, FileMode.Open);
                    BinaryFormatter bin = new BinaryFormatter();
                    products = (Dictionary<string,List<Product>>)bin.Deserialize(st);
                    st.Close();
                    return true;
                }
                catch (IOException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return false;
        }
        #endregion

        #region Destructor
        ~Products()
        { }
        #endregion
    }
}
