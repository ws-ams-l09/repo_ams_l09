﻿/**
 * Author: Carlos Pereira, Fábio Rodrigues, Rui Miranda
 * Date: 17/01/2022
 * Number: 6498, 20079, 21135
 * Version: 1.0
 * Description: Feed Zero Waste
 * */

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using BusinessObjectsDLL;

namespace DadosDLL
{
    [Serializable]
    /// <summary>
    /// Classe usada para armazenar objetos do tipo Cliente 
    /// </summary>
    public class Companies
    {
        #region Attributes
        static List<Company> companies; //Lista de Empresa
        #endregion

        #region Constructors
        /// <summary>
        /// Método estático para definir uma lista de Cliente
        /// </summary>
        static Companies()
        {
            companies = new List<Company>();
        }
        #endregion

        #region Properties
        #endregion

        #region Overrides
        #endregion

        #region Operators
        #endregion

        #region Methods
      
        /// <summary>
        /// Método que procura por uma empresa
        /// </summary>
        /// <param name="cc"></param>
        /// <returns></returns>
        public static Company FindCompany(string name)
        {
            foreach(Company x in companies)
            {
                if (x.NameComp == name)
                    return x;
            }
            return null;
        }

        /// <summary>
        /// Método que verifica se um determinada empresa já existe na lista de empresas
        /// </summary>
        /// <param name="name">Nome da Empresa</param>
        /// <returns></returns>
        public static bool ExistsCompany(string name)
        {
            foreach (Company obj in companies)
            {
                if (obj.NameComp == name)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Método que permite inserir um nova empresa na lista
        /// </summary>
        /// <param name="c">Empresa a inserir</param>
        /// <returns></returns>
        public static bool InsertCompany(Company c)
        {
            if (!ExistsCompany(c.NameComp))
            {
                companies.Add(c);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Método que permite remover da lista de empresas uma determinada empresa
        /// </summary>
        /// <param name="c">Empresa a remover</param>
        /// <returns></returns>
        public static bool RemoveCompany(Company c)
        {
            foreach (Company obj in companies)
            {
                if (obj.IdCompany == c.IdCompany)
                {
                    companies.Remove(c);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Método para listar todas as empresas da lista
        /// </summary>
        public static List<Company> Lista()
        {
            return companies;
        }

        /// <summary>
        /// Método que grava em ficheiro a lista de clientes
        /// </summary>
        /// <param name="fileName">Nome do ficheiro</param>
        /// <returns></returns>
        public static bool GravaFicheiro(string fileName)
        {
            try
            {
                Stream st = File.Open(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(st, companies);
                st.Close();
                return true;
            }
            catch (IOException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Método que permite carregar conteúdo de ficheiro binário
        /// </summary>
        /// <param name="fileName">Nome do ficheiro</param>
        /// <returns></returns>
        public static bool CarregaFicheiro(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    Stream st = File.Open(fileName, FileMode.Open);
                    BinaryFormatter bin = new BinaryFormatter();
                    companies = (List<Company>)bin.Deserialize(st);
                    st.Close();
                    return true;
                }
                catch (IOException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return false;
        }
        #endregion

        #region Destructor
        ~Companies()
        { }
        #endregion
    }
}
